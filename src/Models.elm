module Models exposing (..)

import RemoteData exposing (WebData)


initialModel : Route -> Model
initialModel route =
    { posts = RemoteData.Loading
    , route = route
    }


type alias Model =
    { posts : WebData (List Post)
    , route : Route
    }


type alias PostId =
    String


type alias Post =
    { id : String
    , title : String
    , body : String
    }


type Route
    = HomeRoute
    | AboutRoute
    | ContactRoute
    | PostsRoute
    | PostRoute PostId
    | NotFoundRoute
