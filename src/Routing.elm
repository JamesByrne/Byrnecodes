module Routing exposing (..)

import Navigation exposing (Location)
import Models exposing (Route(..), PostId)
import UrlParser exposing (..)


-- matchers
-- Matches a passed location to a type


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map HomeRoute top
        , map AboutRoute (s "about")
        , map ContactRoute (s "contact")
        , map PostRoute (s "posts" </> string)
        , map PostsRoute (s "posts")
        ]



-- parseLocation
-- Takes a location and matches against the route


parseLocation : Location -> Route
parseLocation location =
    case (parseHash matchers location) of
        Just route ->
            route

        Nothing ->
            NotFoundRoute
