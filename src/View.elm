module View exposing (view)

import Html exposing (Html, div)
import Models exposing (Model, PostId)
import Msgs exposing (Msg)


-- Pages

import Page.Home as Home
import Page.About as About
import Page.Post as Post
import Page.Posts as Posts
import Page.Contact as Contact


-- Views

import Views.Navbar as Navbar
import Views.NotFound as NotFound


view : Model -> Html Msg
view model =
    div []
        [ Navbar.view
        , page model
        ]


page : Model -> Html msg
page model =
    case model.route of
        Models.HomeRoute ->
            Home.view

        Models.AboutRoute ->
            About.view

        Models.ContactRoute ->
            Contact.view

        Models.PostsRoute ->
            Posts.view

        Models.PostRoute id ->
            Post.view

        Models.NotFoundRoute ->
            NotFound.view
