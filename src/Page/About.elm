module Page.About exposing (view)

import Html exposing (Html, text, div)


view : Html msg
view =
    div [] [ text "This is the about page" ]
