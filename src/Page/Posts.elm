module Page.Posts exposing (view)

import Html exposing (Html, div, text)


view : Html msg
view =
    div [] [ text "This is the Posts page" ]
