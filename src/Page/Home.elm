module Page.Home exposing (view)

import Html exposing (Html, text, div, img, p)
import Html.Attributes exposing (class, style)


-- View --


view : Html msg
view =
    heroSection


heroSection : Html msg
heroSection =
    div [ class "container-fluid hero-image" ]
        [ heroImage
        , heroText "Some Text"
        ]


heroImage : Html msg
heroImage =
    rowColWrapper <|
        div []
            []


heroText : String -> Html msg
heroText message =
    rowColWrapper <|
        p []
            [ text message
            ]


rowColWrapper : Html msg -> Html msg
rowColWrapper content =
    div [ class "row" ]
        [ div [ class "col" ]
            [ content
            ]
        ]
