module Update exposing (update)

import Models exposing (Model, Post)
import Msgs exposing (Msg)
import Routing exposing (parseLocation)
import RemoteData


update : Msg -> Model -> ( Model, Cmd msg )
update msg model =
    case msg of
        Msgs.OnFetchPosts response ->
            ( { model | posts = response }, Cmd.none )

        Msgs.OnLocationChange location ->
            let
                newRoute =
                    parseLocation location
            in
                ( { model | route = newRoute }, Cmd.none )
