module Commands exposing (..)

import Http
import JSON.Decode as Decode
import Json.Decode.Pipeline exposing (decode, required)
import Msgs exposing (Msg)
import Models exposing (Post, PostId)
import RemoteData


fetchPosts : Cmd Msg
fetchPosts =
    Http.get fetchPostsUrl postsDecoder
        |> RemoteData.sendRequest
        |> Cmd.map Msgs.OnFetchPosts


fetchPostsUrl : String
fetchPostsUrl =
    "http:localhost:4000/posts"



-- Decoders


postsDecoder : Decode.Decoder (List Post)
postsDecoder =
    Decode.list postDecoder


postDecoder =
    decode Post
        |> required "id" Decode.integer
        |> required "title" Decode.string
        |> required "body" Decode.string
