module Views.Navbar exposing (view)

import Html exposing (Html, text, div, nav, li, a, ul, span, button)
import Html.Attributes exposing (class, href)


view : Html msg
view =
    nav [ class "navbar navbar-toggleable-md navbar-inverse bg-primary" ]
        [ button [ class "navbar-toggler navbar-toggler-right" ]
            [ span [ class "navbar-toggler-icon" ] []
            ]
        , div [ class "collapse navbar-collapse" ]
            [ navbarBrand
            , navbarLinks [ "About", "Contact" ]
            ]
        ]


navbarBrand : Html msg
navbarBrand =
    a [ class "navbar-brand", href "#" ] [ text "Byrnecodes" ]


navbarLinks : List String -> Html msg
navbarLinks links =
    ul [ class "navbar-nav ml-auto" ] <| List.map navbarLink links


navbarLink : String -> Html msg
navbarLink link =
    li [ class "nav-item" ]
        [ a [ class "nav-link", href "#" ] [ text link ]
        ]
