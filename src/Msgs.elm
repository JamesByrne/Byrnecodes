module Msgs exposing (..)

import Models exposing (Post, PostId)
import Navigation exposing (Location)
import RemoteData exposing (WebData)


type Msg
    = OnFetchPosts (WebData (List Post))
    | OnLocationChange Location
