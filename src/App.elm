module App exposing (..)

import Models exposing (Model, initialModel)
import Navigation exposing (Location)
import Msgs exposing (Msg)
import Update exposing (update)
import Routing
import View exposing (view)


-- Init


init : Location -> ( Model, Cmd Msg )
init location =
    let
        currentRoute =
            Routing.parseLocation location
    in
        ( initialModel currentRoute, Cmd.none )



---- PROGRAM ----


main : Program Never Model Msg
main =
    Navigation.program Msgs.OnLocationChange
        { view = view
        , init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        }
